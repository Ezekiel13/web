from flask import Flask, flash, redirect, render_template, request, session, abort
from random import randint
 
app = Flask(__name__)
 
@app.route("/")
def index():
    return "Flask App!"
 
#@app.route("/hello/<string:name>")
@app.route("/hello/<string:name>/")
def hello(name):
#    return name
    quotes = [ "SLAYER's REIGN IN BLOOD album they're a THRASH METAL band",
               "METALLICA's METALLICA album they're a THRASH METAL band",
               "ANTHRAX's PERSISTENCE OF TIME album they're a THRASH METAL band",
               "MEGADETH's PEACE SELLS BUT WHO's BUYING album they're a THRASH METAL band",
               "IRON MAIDEN's SOMEWHERE IN TIME album they're a HEAVY METAL band",
               "DEATH ANGEL's THE ULTRA VIOLENCE album they're a THRASH METAL band",
               "DEATH's SCREAM BLOODY GORE album they're a DEATH METAL band",
               "BRENDON SMALL's GALAKTIKON II  album they're a MELODIC DEATH METAL band",
               "AMON AMARTH's SURTUR RISING  album they're a MELODIC DEATH METAL band",
               "ARCH ENEMY's  RISE OF THE TYRANT and WAR ETERNAL album they're a MELODIC DEATH METAL band",
               "LAMB OF GOD's SACRAMENT album they're a GROOVE METAL band",
               "APOCALYPTICA's  APOCALYPTICA PLAYS METALLICA BY FOUR CELLOS they're a CELLO METAL band",
               "AVATAR's HAIL THE APOCALYPSE album they're a NU-METAL band",
               "ALESTORM's SUNSET ON THE GOLDEN AGE album they're a FOLK METAL band",
               "DETHKLOK's DETHALBUM III album they're a MELODIC DEATH METAL band",
               "CARCASS's SURGICAL STEEL album they're a MELODIC DEATH METAL band",
               "GLORYHAMMER's SPACE 1992: RISE OF THE CHAOS WIZARDS album they're a SYMPHONIC POWER METAL band",
               "GOJIRA's FROM MARS TO SIRIUS album they're a GROOVE METAL band",
               "KATAKLYSM's EPIC THE POETRY OF WAR album they're a MELODIC DEATH METAL band",
               "KILLSWITCH ENGAGE's ALIVE OR JUST BREATHING album they're a METALCORE band",
               "KRISIUN's BLACK FORCE DOMAIN album they're a DEATH METAL band",
               "TRIVIUM's THE SIN AND THE SENTENCE album they're a METALCORE band",
               "VOLBEAT's SEAL THE DEAL AND LET'S BOOGIE album they're a HEAVY METAL band",
               "AS I LAY DYING's THE POWERLESS RISE album they're a METALCORE band",
               "DRAGONFORCE's INHUMAN RAMPAGE album they're a POWER METAL band",
               "ENSIFERUM's UNSUNG HEROES album they're a EPIC FOLK METAL a band",
               "EPICA's THE HOLOGRAPHIC PRINCIPLE album they're a SYPMPHONIC METAL band",
               "KORPIKLAANI's KARKELOT album they're a FOLK METAL band",
               "MASTODON's LEVIATHAN album they're a PROGRESSIVE METAL band",
               "NIGHTWISH's ANGELS FALL FIRST album they're a SYMPHONIC POWER METAL band",
               "NILE's WHAT SHOULD NOT BE UNEARTHED album they're a TECHNICAL DEATH METAL band",
               "ONCE HUMAN's EVOLUTION album they're a GROOVE METAL band",
               "SEMBLANT's LUNAR MANIFESTO album they're a GOTHIC METAL band",
               "SEPULTURA's ROOTS album they're a GROOVE METAL band",
               "SLIPKNOT's IOWA album they're a NU-METAL band",
               "SUFFOCATION's BLOOD OATH album they're a TECHNICAL DEATH METAL band",
               "SUICIDE SILENCE's THE BLACK CROWN album they're a DEATHCORE band",
               "TURISAS's THE VARGIAN WAY album they're a FOLK METAL band"]
    randomNumber = randint(0,len(quotes)-1) 
    quote = quotes[randomNumber] 
 
    return render_template(
        'test.html',**locals())
 
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
