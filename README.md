# Web

This guide assumes you have Flask already installed
1. Open your terminal.
2. Go to directory where you saved the code through the terminal.
3. Once your in the directory of where you saved the code enter python app.py to run it.
4. And then enter this to your browser http://127.0.0.1/hello/Jackson/ you could replace the name Jackson with any name you want.
5. Once the page is running it would suggest a heavy metal bands, album, and will tell you what type of metal genre the band is.
6. Every time you refresh the page it would keep suggesting you various bands.